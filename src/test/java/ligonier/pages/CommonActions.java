package ligonier.pages;

import common.Config;
import utilities.Driver;

import java.util.logging.Logger;

public class CommonActions {
    private Driver driver;
    private final Logger LOGGER = Logger.getLogger(CommonActions.class.getName());


    public CommonActions(Driver driver) {
        this.driver = driver;
    }

    public void launchApp() {
        driver.get(Config.getUrl());
    }

    public void waitForMilliSeconds(int seconds) {
        driver.waitForMilliSeconds(seconds*1000);
    }

}
