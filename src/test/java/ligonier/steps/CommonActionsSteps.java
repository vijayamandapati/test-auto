package ligonier.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import ligonier.pages.CommonActions;
import utilities.Driver;

public class CommonActionsSteps {

    Driver driver;
    private CommonActions commonActions;

    public CommonActionsSteps(Driver driver){
        this.driver = driver;
        commonActions = new CommonActions(driver);
    }

    @When("I launch ligoneir app")
    public void i_launch_ligoneir_app() throws Throwable {
        commonActions.launchApp();
    }

    @When("I debug")
    public void i_debug() throws Throwable {
        System.out.println("This is a debug step");
    }

    @When("I wait for \"([0-9]*)\" seconds")
    public void i_wait_for_seconds(int seconds) throws Throwable {
        commonActions.waitForMilliSeconds(seconds);
    }

    @When("I click on \"([^\"]*)\" element in \"([^\"]*)\" page")
    public void i_click_on_element_in_page(String element, String page) throws Throwable {
        driver.click(page,element);
    }

    @When("I enter \"([^\"]*)\" text in \"([^\"]*)\" element in \"([^\"]*)\" page")
    public void i_enter_text_in_element_in_page(String input, String element, String page) throws Throwable {
        driver.sendKeys(page,element,input);
    }

    @When("^I click on \"([^\"]*)\"$")
    public void i_click_on_link_text(String text) throws Throwable {
        driver.clickOnElementWithText(text);
    }

    @When("^I action click on \"([^\"]*)\"$")
    public void i_action_click_on_link_text(String text) throws Throwable {
        driver.actionClickOnElementWithText(text);
    }

    @When("I click on \"([^\"]*)\" at position \"([^\"]*)\"")
    public void i_click_on_at_position(String text, String position) throws Throwable {
        driver.clickOnElementWithText(text, Integer.parseInt(position));
    }

    @When("I action click on \"([^\"]*)\" at position \"([^\"]*)\"")
    public void i_action_click_on_at_position(String text, String position) throws Throwable {
        driver.actionClickOnElementWithText(text, Integer.parseInt(position));
    }

    @When("I click on \"([^\"]*)\" at position \"([^\"]*)\" with tag \"([^\"]*)\"")
    public void i_click_on_at_position_with_tag(String text, String position, String tagName) throws Throwable {
        driver.clickOnElementWithText(text,tagName, position);
    }

    @When("I click on \"([^\"]*)\" with tag \"([^\"]*)\"")
    public void i_click_on_tag(String text, String tagName) throws Throwable {
        driver.clickOnElementWithText(text,tagName);
    }

    @When("I action click on \"([^\"]*)\" with tag \"([^\"]*)\"")
    public void i_action_click_on_tag(String text, String tagName) throws Throwable {
        driver.actionClickOnElementWithText(text,tagName);
    }

    @Then("I verify if text attribute of \"([^\"]*)\" element in \"([^\"]*)\" page contains \"([^\"]*)\"")
    public void i_verify_if_text_attribute_of_element_in_page_contains(String elementName, String pageName, String textToAssert) throws Throwable {
        driver.assertTextAttributeWithInputText(pageName, elementName, textToAssert);
    }
}
