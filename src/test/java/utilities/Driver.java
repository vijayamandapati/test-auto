package utilities;

import common.Config;
import cucumber.api.Scenario;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class Driver {

    private static WebDriver driver;
    private static final String BROWSER = Config.getBrowser();
    private final Logger LOGGER = Logger.getLogger(Driver.class.getName());
    public ScenarioData scenarioData = new ScenarioData();
    Actions actions;

    public void initialize() {
        if (driver == null)
            createNewDriverInstance();
    }

    private static String ELEMENT_REPOSITORY_PATH = System.getProperty("user.dir") + "\\src\\test\\resources\\ElementsRepository\\";

    /**
     * Creates a new driver instance
     */
    private void createNewDriverInstance() {
        driver = DriverFactory.getDriver(BROWSER);
        actions = new Actions(driver);
    }

    /**
     * Returns the current driver
     *
     * @return driver
     */
    public WebDriver getDriver() {
        return driver;
    }

    /**
     * Quits the current driver session
     */
    public void destroyDriver() {
        LOGGER.info("destroyDriver started");
        try {
            driver.quit();
            driver = null;
        } catch (Exception e) {
            LOGGER.info("Exception: " + e.getMessage());
        }
        LOGGER.info("destroyDriver completed");
    }

    public void get(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }

    /**
     * Embeds a screenshot into a scenario
     *
     * @param scenario Scenario status - Passed or Failed
     * @throws IOException General exception caught to allow for graceful failure
     */
    public void embedScreenshot(Scenario scenario) throws IOException {
        LOGGER.info("embedScreenshot started with scenario '" + scenario + "' " + scenario.getStatus());
        byte[] screenshot;
        try {
            screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            LOGGER.info(somePlatformsDontSupportScreenshots.getMessage());
        }
        LOGGER.info("embedScreenshot completed with scenario '" + scenario + "' " + scenario.getStatus());
    }

    public String returnXpathFromElementRepository(String pageName, String elementName) {
        File[] files = new File(ELEMENT_REPOSITORY_PATH).listFiles();
        String xpath = null;
        for (File file : files) {
            if (file.getName().replace(".json", "").equals(pageName)) {
                try {
                    Object object = new JSONParser().parse(new FileReader(file.getPath()));
                    JSONObject jsonObject = (JSONObject) object;
                    xpath = jsonObject.get(elementName).toString();
                    break;
                } catch (IOException io) {
                    io.printStackTrace();
                } catch (ParseException pr) {
                    pr.printStackTrace();
                }
            }
        }
        return xpath;
    }

    public WebElement returnElementFromElementRepository(String pageName, String elementName) {
        return driver.findElement(By.xpath(returnXpathFromElementRepository(pageName, elementName)));
    }

    public List<WebElement> returnListOfElementsFromElementRepository(String pageName, String elementName) {
        return driver.findElements(By.xpath(returnXpathFromElementRepository(pageName, elementName)));
    }

    public void click(String pageName, String elementName) {
        WebElement element = returnElementFromElementRepository(pageName, elementName);
        try {
            waitForElementClickable(element);
            element.click();
        } catch (StaleElementReferenceException se) {
            WebElement staleElement = driver.findElement(By.xpath(returnXpathFromElementRepository(pageName, elementName)));
            staleElement.click();
        } catch (WebDriverException we) {
            actions.click(returnElementFromElementRepository(pageName, elementName)).perform();
        }
    }

    public void sendKeys(String pageName, String elementName, String input) {
        try {
            sendKeys(returnElementFromElementRepository(pageName, elementName), input);
        } catch (StaleElementReferenceException se) {
            sendKeys(returnElementFromElementRepository(pageName, elementName), input);
        }
    }

    private void sendKeys(WebElement element, String input) {
        waitForElementClickable(element);
        element.click();
        element.clear();
        element.sendKeys(input);
    }

    public WebElement returnElementContainsText(String text) {
        return driver.findElement(By.xpath("//*[contains(text(),'" + text + "')]"));
    }

    public WebElement returnElementContainsText(String text, int position) {
        return driver.findElement(By.xpath("//*[contains(text(),'" + text + "')]['" + position + "']"));
    }

    public WebElement returnElementContainsText(String text, String tagName) {
        return driver.findElement(By.xpath("//" + tagName + "[contains(text(),'" + text + "')]"));
    }

    public WebElement returnElementContainsText(String text, String tagName, String position) {
        return driver.findElement(By.xpath("//" + tagName + "[contains(text(),'" + text + "')][" + position + "]"));
    }

    public void clickOnElementWithText(String text) {
        returnElementContainsText(text).click();
    }

    public void actionClickOnElementWithText(String text) {
        actions.click(returnElementContainsText(text)).perform();
    }

    public void clickOnElementWithText(String text, int position) {
        returnElementContainsText(text, position).click();
    }

    public void actionClickOnElementWithText(String text, int position) {
        try {
            actions.click(returnElementContainsText(text, position)).perform();
        } catch (JavascriptException js) {
            jsClick(returnElementContainsText(text, position));
        }
    }

    public void jsClick(WebElement element) {
        String mouseOverScript = "arguments[0].click();";
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript(mouseOverScript, element);
    }

    public void clickOnElementWithText(String text, String tagName) {
        try {
            returnElementContainsText(text, tagName).click();
        } catch (ElementNotInteractableException en) {
            actions.click(returnElementContainsText(text, tagName)).perform();
        }
    }

    public void actionClickOnElementWithText(String text, String tagName) {
        actions.click(returnElementContainsText(text, tagName)).perform();
    }

    public void clickOnElementWithText(String text, String tagName, String position) {
        returnElementContainsText(text, tagName, position).click();
    }

    public void waitForElementClickable(WebElement element, int timeInSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeInSeconds);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = driver.findElement(extractByLocatorFromWebElement(element));
            webDriverWait.until(ExpectedConditions.elementToBeClickable(staleElement));
        }
    }

    public void waitForElementToBeVisible(WebElement element, int timeInSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeInSeconds);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOf(element));
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = driver.findElement(extractByLocatorFromWebElement(element));
            webDriverWait.until(ExpectedConditions.visibilityOf(staleElement));
        }
    }

    public By extractByLocatorFromWebElement(WebElement element) {
        String elementContent = element.toString();
        String unCleanXpath = elementContent.substring(elementContent.indexOf("xpath:") + 6);
        String cleanXpath = unCleanXpath.substring(0, unCleanXpath.length() - 1);
        return By.xpath(cleanXpath);
    }

    public void waitForMilliSeconds(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            LOGGER.info(e.toString());
        }
    }

    public void waitForElementClickable(WebElement element) {
        waitForElementClickable(element, 30);
    }

    public void waitForElementToBeVisible(WebElement element) {
        waitForElementToBeVisible(element, 30);
    }

    public void saveTextAttribute(WebElement element, String key) {
        waitForElementToBeVisible(element);
        String text = element.getText().trim();
        scenarioData.setData(key, text);
        LOGGER.info("Text attribute of the element is: " + text);
    }

    public void saveValueAttribute(WebElement element, String key) {
        waitForElementToBeVisible(element);
        String value = element.getAttribute("value").trim();
        scenarioData.setData(key, value);
        LOGGER.info("Value attribute of the element is: " + value);
    }

    public void saveTitleAttribute(WebElement element, String key) {
        waitForElementToBeVisible(element);
        String title = element.getAttribute("title").trim();
        scenarioData.setData(key, title);
        LOGGER.info("Title attribute of the element is: " + title);
    }

    public void saveTextAttribute(String pageName, String elementName, String key) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        saveTextAttribute(returnElementFromElementRepository(pageName, elementName), key);
    }

    public void saveValueAttribute(String pageName, String elementName, String key) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        saveValueAttribute(returnElementFromElementRepository(pageName, elementName), key);
    }

    public void saveTitleAttribute(String pageName, String elementName, String key) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        saveTitleAttribute(returnElementFromElementRepository(pageName, elementName), key);
    }

    public void assertTextAttributeWithInputText(WebElement element, String textToAssert) {
        waitForElementToBeVisible(element);
        String text = element.getText().trim();
        Assert.assertTrue("FAIL: Text Found: " + text + " does not contain: " + textToAssert, text.contains(textToAssert));
        LOGGER.info("Text attribute of the element is: " + text);
    }

    public void assertValueAttributeWithInputText(WebElement element, String textToAssert) {
        waitForElementToBeVisible(element);
        String value = element.getAttribute("value").trim();
        Assert.assertTrue("FAIL: Text Found: " + value + " does not contain: " + textToAssert, value.contains(textToAssert));
        LOGGER.info("Text attribute of the element is: " + value);
    }

    public void assertTitleAttributeWithInputText(WebElement element, String textToAssert) {
        waitForElementToBeVisible(element);
        String title = element.getAttribute("title").trim();
        Assert.assertTrue("FAIL: Text Found: " + title + " does not contain: " + textToAssert, title.contains(textToAssert));
        LOGGER.info("Text attribute of the element is: " + title);
    }

    public void assertTextAttributeWithInputText(String pageName, String elementName, String textToAssert) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        assertTextAttributeWithInputText(returnElementFromElementRepository(pageName, elementName), textToAssert);
    }

    public void assertValueAttributeWithInputText(String pageName, String elementName, String textToAssert) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        assertValueAttributeWithInputText(returnElementFromElementRepository(pageName, elementName), textToAssert);
    }

    public void assertTitleAttributeWithInputText(String pageName, String elementName, String textToAssert) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        assertTitleAttributeWithInputText(returnElementFromElementRepository(pageName, elementName), textToAssert);
    }

    public void assertTextAttributeWithPreviouslySavedValue(String pageName, String elementName, String key) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        assertTextAttributeWithInputText(returnElementFromElementRepository(pageName, elementName), scenarioData.getData(key));
    }

    public void assertValueAttributePreviouslySavedValue(String pageName, String elementName, String key) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        assertValueAttributeWithInputText(returnElementFromElementRepository(pageName, elementName), scenarioData.getData(key));
    }

    public void assertTitleAttributePreviouslySavedValue(String pageName, String elementName, String key) {
        waitForElementToBeVisible(returnElementFromElementRepository(pageName, elementName));
        assertTitleAttributeWithInputText(returnElementFromElementRepository(pageName, elementName), scenarioData.getData(key));
    }

}
