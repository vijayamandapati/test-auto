@createaccount
Feature: Create Account

  @52369
  Scenario Outline: Create account test one
    When I launch ligoneir app
    And  I click on "Create Account"
    And  I click on "Ministries" with tag "cufontext"
    And  I enter "<Email>" text in "Email" element in "create account" page
    And  I enter "<Password>" text in "Password" element in "create account" page
    And  I enter "<First Name>" text in "First Name" element in "create account" page
    And  I enter "<Last Name>" text in "Last Name" element in "create account" page
    And  I wait for "10" seconds

    Examples:
      | Email              | Password | First Name    | Last Name |
      | lorik.sm@gmail.com | Lorik    | Lorik smolica | smolica   |
      | lorik.sm@gmail.com | Lorik    | Lorik smolica | smolica   |
