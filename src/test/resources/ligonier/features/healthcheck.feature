Feature: Health Checks

  Scenario: Health Check in production
    When I launch ligoneir app
    And  I verify if text attribute of "Resource Header" element in "home" page contains "DAILY RESOURCES "
    And  I action click on "Learn" with tag "cufontext"
    And  I verify if text attribute of "Header" element in "learn" page contains "Ways to Learn at Ligonier.org"
    And  I action click on "Ministries" with tag "cufontext"
    And  I verify if text attribute of "Header" element in "ministries" page contains "Helping You Grow in the"
    And  I verify if text attribute of "Header" element in "ministries" page contains "Knowledge of God and His Holiness"
    And  I action click on "Events" with tag "cufontext"
    And  I verify if text attribute of "Header" element in "events" page contains "Upcoming Events"
    And  I action click on "Give" with tag "cufontext"
    And  I verify if text attribute of "Header" element in "give" page contains "Your Generosity Fuels Our Ministry"
    And  I action click on "Partner With Us" at position "2"
    And  I verify if text attribute of "Header" element in "give" page contains "Become a Monthly Partner"
    And  I action click on "Non-Cash Giving"
    And  I verify if text attribute of "Header" element in "give" page contains "Further the Mission with a Non-Cash Gift"
    And  I action click on "Store" with tag "cufontext"
    And  I verify if text attribute of "Header" element in "store" page contains "Featured Resources"